# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: ___

- Nom, Prénom, email: ___

## Question 1

Command executées (sur machine perso) :
sudo adduser toto
sudo adduser toto anthony

Non il peut uniquement le lire, toto n'a pas le droit d'écriture (pas de w dans la parti user des droits).

## Question 2

x correspond aux droits d'ouvrir le repertoire et d'accès à ses fichiers.

En retirant x au groupe, toto ne peut pas accéder au dossier, simplement car il n'a pas les droits d'execution liée à son appartenance au groupe anthony.

En tentant de ce même dossier on vois uniquement de "????", car il n'a pas le droit d'ouvrir le dossier et donc pas le droit de lister ses fichiers.

EUID : 1001; EGID : 1001; RUID : 1001, RGID : 1001;



## Question 3

Avant de set-user-id sur l'executable on ne peut pas ouvrir le fichier et les ids ont les valeurs suivantes : 
EUID : 1001; EGID : 1001; RUID : 1001, RGID : 1001;
Après avoir set-user-id sur l'executable on peut ouvrir le fichier et les ids ont les valeurs suivantes : 
EUID : 1000; EGID : 1000; RUID : 1001, RGID : 1001;

## Question 4

Les differents ids sont :
EUID : 1001; EGID : 1001;
Donc on peut voir que ça ne fonctionne pas avec python. La raison est que le programme est exécuté par l'interpréteur python3 sur lequel nous n'avons pas fait de set_uid.

## Question 5
ls -al /usr/bin/chfn donne :

-rwsr-xr-x 1 root root 85064 mai   28  2020 /usr/bin/chfn


## Question 6

Les mots de passe sont stockés dans /etc/shadow


## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








