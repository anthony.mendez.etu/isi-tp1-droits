#!/bin/bash

## Création des groupes
adduser little_admin
usermod -a -G group_a little_admin 
usermod -a -G group_b little_admin 
groupadd group_a
groupadd group_b
groupadd group_c

## Création directories
mkdir dir_a
mkdir dir_b
mkdir dir_c

## Affectation des droits du groupe_a sur le dir_a + sticky bit
chmod +t dir_a
chmod g+s dir_a
chmod g+w dir_a
chmod o-rwx dir_a
chown little_admin:group_a dir_a

## Affectation des droits du groupe_b sur le dir_b + sticky bit
chmod +t dir_b
chmod g+s dir_b
chmod g+w dir_b
chmod o-rwx dir_a
chown little_admin:group_a dir_b

## Affectation des droits sur le dir_c + sticky bit
chmod +t dir_c
chmod g+x dir_c
chmod o-rwx dir_c
chown little_admin:group_c dir_c


