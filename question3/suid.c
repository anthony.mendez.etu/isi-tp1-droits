#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[]){
    FILE *f;

    printf("EUID : %d; EGID : %d; RUID : %d, RGID : %d;\n", geteuid(), getegid(), getuid(), getgid());
 
    f = fopen("mydir/mydata.txt", "r");
    
    
    if (f == NULL) {
        perror("Cannot open file");
        exit(0);
    }
    printf("File opens correctly\n");
    
       char line [ 128 ]; /* or other suitable maximum line size */

    while ( fgets ( line, sizeof line, f ) != NULL ) /* read a line */
    {
        fputs ( line, stdout ); /* write the line */
    }
    fclose(f);
    exit(1);
}
